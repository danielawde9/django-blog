# Blog Django Project



# Initial Start

Create a folder

    $ cd ~/Desktop
	$ mkdir blog
	$ cd blog
	$ pipenv install django
	$ pipenv shell

it will create a pipenv environment start a project and don't forget the "." at the end of the code

	(blog) $ django-admin startproject blog_project .
	(blog) $ python manage.py startapp blog

Then execute the migrate command to create an initial database based on Django’s default settings.

	(blog) $ python manage.py migrate

Run server

	(blog) $ python manage.py runserver

Tell django about the new app `blog` by adding it to the top of `INSTALLED_APPS` in the blog_project/settings.py

**blog_project/settings.py**

    INSTALLED_APPS = [
    'blog.apps.BlogConfig', # new
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    '

# Models

A model is the single, definitive source of information about your data. It contains the essential fields and behaviors
of the data you’re storing. Generally, each model maps to a **single database table.**

We will “model” the characteristics of the data in our database.

- Each model is a Python class that subclasses django.db.models.Model.
- Each attribute of the model represents a database field.
- With all of this, Django gives you an automatically-generated database-access API

**blog/models.py**

	# blog/models.py
	from django.db import models

    class Post(models.Model):
        title = models.CharField(max_length=200)
        
        author = models.ForeignKey(
            'auth.User',
            on_delete=models.CASCADE,
        )

        body = models.TextField()
    
        def __str__(self):
            return self.title

> def __str__ will return by default Post object #
> so to fix that we go in the blog/models.py and add the following
> def __str__(self):
return self.title


Creating a subclass of models.Model called Post .

For the author field we’re using a ForeignKey which allows for a many-to-one relationship. This means that a given user
can be the author of many different blog posts but not the other way around. The reference is to the built-in User model
that Django provides for authentication. For all many-to-one relationships such as a ForeignKey we must also specify an
on_delete option.

## Activating Models

	(blog) $ python manage.py makemigrations blog
	(blog) $ python manage.py migrate blog

### Django Admin

in the /admin/ page

	(blog) $ python manage.py createsuperuser
	Username (leave blank to use 'wsv'): wsv
	Email:
	Password:
	Password (again):
	Superuser created successfully.

## Register the blog app in the admin page

**blog/admin.py**

	from django.contrib import admin
	from .models import Post

	admin.site.register(Post)

# Pattern in adding new pages

```mermaid
graph LR
A[Templates] --> B((View))
B --> D{URL}
```

In order to view all values of the Blog DB model, list all the content we will use built in generic class-based
ListView.

## Templates

Create a project-level directory called templates, and a home.html template file.

	(blog) $ mkdir templates
	(blog) $ touch templates/home.html
	(blog) $ touch templates/base.html

Add the settings.py directory of the template

**blog_project/settings.py**

	TEMPLATES = [
		{
			...
			'DIRS': [os.path.join(BASE_DIR, 'templates')], # new
			...
		},
	]

## Template Base

    <!-- templates/base.html -->
    <html>
    <head>
        <title>Django blog</title>
    </head>
    <body>
        <header>
            <h1><a href="{% url 'home' %}">Django blog</a></h1>
        </header>
        <div>
            {% block content %}
            {% endblock content %}
        </div>
    </body>
    </html>

## Template Home

In our templates file home.html we can use the Django Templating Language’s for a loop to list all the objects in
object_list Why **object_list** ? This is the name of the variable that ListView returns to us.

    <!-- templates/home.html -->
    {% extends 'base.html' %}
    
    {% block content %}
        {% for post in object_list %}
            <div class="post-entry">
                <h2><a href="">{{ post.title }}</a></h2>
                <p>{{ post.body }}</p>
            </div>
        {% endfor %}
    {% endblock content %}

In order to fix the naming of **object_list** we need to rename the return of the views of the `blog/views.py`
called `context_object_name = 'what ever name you want'` here we called `all_blog_list`

    <!-- templates/home.html -->
    {% extends 'base.html' %}
    
    {% block content %}
        {% for post in all_blog_list %}
            <div class="post-entry">
                <h2><a href="">{{ post.title }}</a></h2>
                <p>{{ post.body }}</p>
            </div>
        {% endfor %}
    {% endblock content %}

## Views

**In the blog/views.py**

	# blog/views.py
	from django.views.generic import ListView
	from .models import Post
	
	class BlogListView(ListView):
		model = Post
		template_name = 'home.html'
		context_object_name = 'all_blog_list'

> Internally ListView returns an object called object_list that we want to display in our template.

Now in the views it will get all the models from DB that we added from the `blog/models.py`

## URLS

```mermaid
graph LR
A[App Level URL] --  Include all the links--> B[Project Level URL]

```

First we create and add the path of home in the Post app, create a new one `

    touch blog/urls.py 

Then in **blog/urls.py**

	# blog/urls.py
	from django.urls import path
	from .views import BlogListView

	urlpatterns = [
		path('', BlogListView.as_view(), name='home'),
	]

Then we add the path of the `home` to `blog_project/urls.py` that includes the URL blog that have all the links inside
it for the blog app

	# blog_project/urls.py
	from django.contrib import admin
	from django.urls import path, include # new
	urlpatterns = [
		path('admin/', admin.site.urls),
		path('', include('blog.urls')), # new
	]

# Static

    (blog) $ mkdir static

We can update settings.py with a one-line change for STATICFILES_DIRS . Add it at the bottom of the file below the entry
for STATIC_URL .

    # blog_project/settings.py
    STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static')]

Now create a css folder within static and add a new base.css file in it. Command Line

    (blog) $ mkdir static/css
    (blog) $ touch static/css/base.css

For example

    /* static/css/base.css */
    header h1 a {
    color: red;
    }

add `{% load static %}` to the top of base.html

Because our other templates inherit from `base.html` we only have to add this once. Include a new line at the bottom of
the <head></head> code that explicitly references our new base.css file.

    <!-- templates/base.html -->
    {% load static %}
    <html>
    <head>
        <title>Django blog</title>
        <link href="{% static 'css/base.css' %}" rel="stylesheet">
    </head>

# Adding post detail

## View

Start with the view. We can use the generic class-based DetailView to simplify things. At the top of the file add
DetailView to the list of imports and then create our new view called BlogDetailView .

    # blog/views.py
    from django.views.generic import ListView, DetailView # new
    from .models import Post

    class BlogListView(ListView):
        model = Post
        template_name = 'home.html'
    class BlogDetailView(DetailView): # new
        model = Post
        template_name = 'post_detail.html'

In this new view we define the model we’re using, Post , and the template we want it associated with, post_detail.html .
By default DetailView will provide a context object we can use in our template called either object or the lowercased
name of our model, which would be post . Also, DetailView expects either a primary key or a slug passed to it as the
identifier.

## Template

    (blog) $ touch templates/post_detail.html

then add

    <!-- templates/post_detail.html -->
    {% extends 'base.html' %}
    
    {% block content %}
        <div class="post-entry">
            <h2>{{ post.title }}</h2>
            <p>{{ post.body }}</p>
        </div>
    {% endblock content %}

Or we can add object instead of post it would look like this and work the same

    <!-- templates/post_detail.html -->
    {% extends 'base.html' %}
    
    {% block content %}
        <div class="post-entry">
            <h2>{{ object.title }}</h2>
            <p>{{ object.body }}</p>
        </div>
    {% endblock content %}

Or we can add in the view class `blog/views.py` in the class name `context_object_name = 'all_blog_list'` which we can
use in the template

## URL

in the `blog/url.py` in `urlpattern` add

    path('post/<int:pk>/', BlogDetailView.as_view(), name='post_detail'), # new

Next is the primary key for our post-entry which will be represented as an integer `<int:pk>`

Django automatically adds an auto-incrementing primary key to our database models.

So while we only declared the fields title , author , and body on our Post model, under-the-hood Django also added
another field called id , which is our primary key. We can **access it as either id or pk**.

To be able to navigate to the post, in `templates/home.html` in anchor add the href `{% url 'post_detail' post.pk %}`

    <!-- templates/home.html -->
    {% extends 'base.html' %}
    
    {% block content %}
        {% for post in object_list %}
            <div class="post-entry">
                <h2><a href="{% url 'post_detail' post.pk %}">{{ post.title }}</a></h2>
                <p>{{ post.body }}</p>
            </div>
        {% endfor %}
    {% endblock content %}

The `url 'post_detail'` came from the URLConf we did in `url.py` name

Refresh the page and make sure it works

# 
# Forms 

First we will update base template to enter a link called new post `<a href="{% url 'post_new' %}">+ New Blog Post</a>`
Then we create a view 
    
## Create View Form 

    # blog/views.py
    class BlogCreateView(CreateView): # new
        model = Post
        template_name = 'post_new.html'
        fields = '__all__'

Then we add a URLConf of the new View created

    # blog/urls.py
    ...
    path('post/new/', BlogCreateView.as_view(), name='post_new'), # new

Create our new template `touch templates/post_new.html`

    <!-- templates/post_new.html -->
    {% extends 'base.html' %}
    {% block content %}
        <h1>New post</h1>
            <form action="" method="post">{% csrf_token %}
            {{ form.as_p }}
            <input type="submit" value="Save" />
        </form>
    {% endblock content %}

- Add a `{% csrf_token %}` which Django provides to protect our form from cross-
site scripting attacks. You should use it for all your Django forms.
- Then to output our form data we use `{{ form.as_p }}` which renders it within
paragraph <p> tags.

When running the new post it gave us `ImproperConfigured` so we should use `get_absolute_url` It sets a canonical URL for an object so even if
the structure of your URLs changes in the future, the reference to the specific object
is the same. In short, you should add a `get_absolute_url()` and `__str__()` method to
each **model** you write.

in `blog/models.py` after `__str__`

    def get_absolute_url(self): # new
        from django.urls import reverse
        return reverse('post_detail', args=[str(self.id)])

So it would be inpendent of the URL path and follow the `URLConf` used in URLs `post/<int:pk>/`

## Update View Form

Let's add a new link inside the `post_detail.html` to be able to edit invidiual post

    <a href="{% url 'post_edit' post.pk %}">+ Edit Blog Post</a>

### Template

in `post_edit.html`

    <!-- templates/post_edit.html -->

    {% extends 'base.html' %}

    {% block content %}
        <h1>Edit Post</h1>
        <form action="" method="post">{% csrf_token %%}
            {{form.as_p}}
            <input type="submit" value="Update">
        </form>

    {% endblock content %}

Django’s csrf_token for security, form.as_p
to display our form fields with paragraph tags

### View
in `blog/views.py`

    class BlogEditView(UpdateView):
        model = Post
        template_name = "post_edit.html"
        fields = ['title', 'body']


### URL
in `blog/urls.py`

    path('post/<int:pk>/edit/',BlogUpdateView.as_view(), name='post_edit'), # new

## Delete View Form

Let's add a new link inside the `post_detail.html` to be able to delete invidiual post

    <p><a href="{% url 'post_delete' post.pk %}">+ Delete Blog Post</a></p>

### Template

    (blog) $ touch templates/post_delete.html

Code

    <!-- templates/post_delete.html -->
    {% extends 'base.html' %}
    {% block content %}
        <h1>Delete post</h1>
        
        <form action="" method="post">{% csrf_token %}
            <p>Are you sure you want to delete "{{ post.title }}"?</p>
            <input type="submit" value="Confirm" />
        </form>
    {% endblock content %}

### View 
        
    class BlogDeleteView(DeleteView):
        model = Post
        template_name = "post_delete.html"
        success_url = reverse_lazy('home')


### URL

urls.py

    path('post/<int:pk>/delete/', BlogDeleteView.as_view(), name='post_delete'),

# User Auth

## Login
Whenever you create a new project, by default Django installs the auth app, which
provides us with a User object containing:
-username
- password
- email
- first_name
- last_name

Django provides us with a default view for a log in page via LoginView. All we need to
add are a project-level urlpattern for the auth system, a log in template, and a small
update to our `settings.py` file.

in **project level URL** add

    path('accounts/', include('django.contrib.auth.urls')), # new

with the import inlude from urls

As the LoginView documentation notes, by default Django will look within a templates
folder called registration for a file called login.html for a log in form. So we need to
create a new directory called registration and the requisite file within it.

    (blog) $ mkdir templates/registration
    (blog) $ touch templates/registration/login.html

in `login.html`

    {% extends 'base.html' %}

    {% block content %}
    <h2>Login</h2>
    <form method="post">
        {% csrf_token %}
        {{ form.as_p }}
        <button type="submit">Login</button>
    </form>
    {% endblock content %}

The form’s contents are outputted between paragraph tags
thanks to {{ form.as_p }} and then we add a “submit” button.

The final step is we need to specify where to redirect the user upon a successful log in.
We can set this with the LOGIN_REDIRECT_URL setting. At the bottom of the settings.py
file add the following:

    # blog_project/settings.py
    LOGIN_REDIRECT_URL = 'home'

## Updated homepage

Let’s update our base.html template so we display a message to users whether they
are logged in or not. We can use the is_authenticated attribute for this.

    {% if user.is_authenticated %}
    <p>Hi {{ user.username }}!</p>
    {% else %}
    <p>You are not logged in.</p>
    <a href="{% url 'login' %}">Log In</a>
    {% endif %}

for logging out under user is authenticated

    <p><a href="{% url 'logout' %}">Log out</a></p>

THats all for logging out 

Update settings.py to provide a redirect link which is called, appropriately, LOGOUT_-
REDIRECT_URL .

    LOGOUT_REDIRECT_URL = 'home' # new

## Sign up

We need to write our own view for a sign up page to register new users, but Django
provides us with a form class, UserCreationForm, to make things easier. By default it
comes with three fields: username , password , and password

We will create a new app for user sign up page

    (blog) $ python manage.py startapp accounts

Add it to settings.py 

    'accounts.apps.AccountsConfig', # new


Next add a project-level `blog_project` url pointing to this new app directly below where we include
the built-in auth app.

    path('accounts/', include('accounts.urls')), # new

### URL 
Create the urls.py for the account app

    (blog) $ touch accounts/urls.py

add the urlpatterns

    # acounts/urls.py
    from django.urls import path
    from .views import SignUpView
    urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    ]

### VIEW


    # accounts/views.py
    from django.contrib.auth.forms import UserCreationForm
    from django.urls import reverse_lazy
    from django.views import generic

    class SignUpView(generic.CreateView):
        form_class = UserCreationForm
        success_url = reverse_lazy('login')
        template_name = 'signup.html'

Why use reverse_lazy here instead of reverse ? The reason is that for all generic class-
based views the URLs are not loaded when the file is imported, so we have to use the
lazy form of reverse to load them later when they’re available.


### Template

    (blog) $ touch templates/signup.html

    <!-- templates/signup.html -->
    {% extends 'base.html' %}
    {% block content %}
        <h2>Sign up</h2>
        <form method="post">
            {% csrf_token %}
            {{ form.as_p }}
            <button type="submit">Sign up</button>
        </form>
    {% endblock content %}



# TestCase
Whenever you’re testing views you should use Client() 

    from django.contrib.auth import get_user_model
    from django.test import TestCase
    
    # Create your tests here.
    from django.urls import reverse
    
    from blog.models import Post
    
    # for testing forms
    class BlogTests(TestCase):
        def setUp(self):
            self.user = get_user_model().objects.create_user(
                username='testuser',
                email='test@email.com',
                password='secret'
            )

        self.post = Post.objects.create(
            title='A good title',
            body='Nice body content',
            author=self.user,
        )

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/post/ /')

    def test_string_representation(self):
        post = Post(title='A sample title')
        self.assertEqual(str(post), post.title)

    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'A good title')
        self.assertEqual(f'{self.post.author}', 'testuser')
        self.assertEqual(f'{self.post.body}', 'Nice body content')

    def test_post_list_view(self):
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code,200)
        self.assertContains(response, 'Nice body content')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/10000/ ')
        self.assertEqual(response.status_code,200)
        self.assertEqual(no_response.status_code,404)
        self.assertContains(response, 'A good title')
        self.assertTemplateUsed(response, 'post_detail.html')

    def test_post_create_view(self): # new
        response = self.client.post(reverse('post_new'), {
            'title': 'New title',
            'body': 'New text',
            'author': self.user,
        })
        self.assertEqual(response.status_code,200)
        self.assertContains(response, 'New title')
        self.assertContains(response, 'New text')

    def test_post_update_view(self): # new
        response = self.client.post(reverse('post_edit', args=' '), {
            'title': 'Updated title',
            'body': 'Updated text',
        })
        self.assertEqual(response.status_code,302)
    def test_post_delete_view(self): # new
        response = self.client.get(
        reverse('post_delete', args=' '))
        self.assertEqual(response.status_code,200)

    
    # Testing links code
    class HomePageViewTest(TestCase):  
	      
	    # initial setUp function   
	    def setUp(self):  
	        Blog.objects.create(text='this is another test')  
	  
	    def test_view_url_exists_at_proper_location(self):  
	        resp = self.client.get('/')  
	        self.assertEqual(resp.status_code,200)  
	  
	    def test_view_url_name(self):  
	        resp = self.client.get(reverse('home'))  
	        self.assertEqual(resp.status_code, 200)  
	  
	    def test_view_uses_correct_template(self):  
	        resp = self.client.get(reverse('home'))  
	        self.assertEqual(resp.status_code, 200)  
	        self.assertTemplateUsed(resp, 'home.html')
	

**Run the code**

	(blog) $ python manage.py test

# Git

First we init all the global

    (blog) $ git init
	(blog) $ git add -A
	(blog) $ git commit -m 'initial commit'
	(blog) $ git remote add origin git@gitlab.com:danielawde9/blog-app.git
	(blog) $ git push -u origin master

## If you want to remove folder form git and keep it locally 

    # Remove the file from the repository
    git rm --cached - r .idea/
    
    # now update your gitignore file to ignore this folder
    echo '.idea' >> .gitignore
    
    # add the .gitignore file
    git add .gitignore
    
    git commit -m "Removed .idea files"
    git push origin origin

## Global .gitignore for your machine

    touch ~/.gitignore

Nano into it and add

    .idea

then in terminal 

    git config --global core.excludesfile '~/.gitignore'

Now all future git repo will ignore .idea folder

## Generate SSH key

	ssh-keygen -t ed25519 -C “Comment”

Then add .pub which is the public key to gitlab

# Heroku

- update Pipfile.lock
- new Procfile
- install gunicorn
- update settings.py

## Check `Pipfile` python required version

	[requires]
	python_version = "3.7"

Run `pipenv lock` to generate the appropriate `Pipfile.lock` .

### Procfile

Then create a Procfile which tells Heroku how to run the remote server where our code will live.

	touch Procfile

For now, we’re telling Heroku to use gunicorn as our production server and look in our blog_project.wsgi file for
further instructions.

Inside the Procfile

	web: gunicorn blog_project.wsgi --log-file -

Next install gunicorn which we’ll use in production while still using Django’s internal server for local development
use.

	(blog) $ pipenv install gunicorn

Finally, update `ALLOWED_HOSTS` in our settings.py file.

	# blog_project/settings.py
	ALLOWED_HOSTS = ['*']

## Heroku Deployment

Make sure to login

	(blog) $ heroku login
	(blog) $ heroku create

Now we need to add a “hook” for Heroku within a git. This means that git will store both our settings for pushing code
to Bitbucket and to Heroku.

My app name is `calm-badlands-09889` so the command will be

	(blog) $ heroku git:remote -a calm-badlands-09889

Tell Heroku to ignore static files which we’ll cover in-depth when deploying our Blog app later in the book.

	(blog) $ heroku config:set DISABLE_COLLECTSTATIC=1

Push the code to Heroku and add free scaling, so it’s actually running online, otherwise the code is just sitting there.

	(blog) $ git push heroku master
	(blog) $ heroku ps:scale web=1

## Check Chapter 7 for deplyong in heruko with Static files